# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :rojak,
  ecto_repos: [Rojak.Repo]

# Configures the endpoint
config :rojak, Rojak.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "e87cgm+tGteKhc2gyDwhCfgfvauClr5yH7ipLoLyreO7BgApds8QGuElDBQU3p3h",
  render_errors: [view: Rojak.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Rojak.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :guardian, Guardian,
  issuer: "Rojak",
  ttl: { 30, :days },
  allowed_drift: 2000,
  secret_key: "e87cgm+tGteKhc2gyDwhCfgfvauClr5yH7ipLoLyreO7BgApds8QGuElDBQU3p3h",
  serializer: Rojak.GuardianSerializer

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
