defmodule Rojak.SessionsControllerTest do
  use Rojak.ConnCase

  alias Rojak.Sessions
  @valid_attrs %{password: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, sessions_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    sessions = Repo.insert! %Sessions{}
    conn = get conn, sessions_path(conn, :show, sessions)
    assert json_response(conn, 200)["data"] == %{"id" => sessions.id,
      "password" => sessions.password}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, sessions_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, sessions_path(conn, :create), sessions: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Sessions, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, sessions_path(conn, :create), sessions: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    sessions = Repo.insert! %Sessions{}
    conn = put conn, sessions_path(conn, :update, sessions), sessions: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Sessions, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    sessions = Repo.insert! %Sessions{}
    conn = put conn, sessions_path(conn, :update, sessions), sessions: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    sessions = Repo.insert! %Sessions{}
    conn = delete conn, sessions_path(conn, :delete, sessions)
    assert response(conn, 204)
    refute Repo.get(Sessions, sessions.id)
  end
end
