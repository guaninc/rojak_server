defmodule Rojak.ItemTest do
  use Rojak.ModelCase

  alias Rojak.Item

  @valid_attrs %{description: "some content", halal: 42, image: "some content", name: "some content", race: "some content", s: "some content", veg: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Item.changeset(%Item{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Item.changeset(%Item{}, @invalid_attrs)
    refute changeset.valid?
  end
end
