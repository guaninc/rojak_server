defmodule Rojak.UserTest do
  use Rojak.ModelCase

  alias Rojak.User

  @valid_attrs %{beef: 42, email: "some content", halal: 42, name: "some content", nuts: 42, password: "some content", veg: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
