defmodule Rojak.SessionsView do
  use Rojak.Web, :view

  def render("index.json", %{email: email}) do
    %{data: render_many(email, Rojak.SessionsView, "sessions.json")}
  end

  def render("show.json", %{sessions: sessions}) do
    render_one(sessions, Rojak.SessionsView, "sessions.json")
  end

  def render("sessions.json", %{sessions: sessions}) do
    %{sid: sessions.session_id,
      name: sessions.user.name,
      email: sessions.user.email}
  end
end
