defmodule Rojak.UserView do
  use Rojak.Web, :view

  def render("index.json", %{users: users}) do
    %{data: render_many(users, Rojak.UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, Rojak.UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id,
      email: user.email,
      name: user.name,
      password: user.password,
      halal: user.halal,
      veg: user.veg,
      beef: user.beef,
      nuts: user.nuts}
  end
end
