defmodule Rojak.ItemView do
  use Rojak.Web, :view

  def render("index.json", %{items: items}) do
    %{data: render_many(items, Rojak.ItemView, "item.json")}
  end

  def render("show.json", %{item: item}) do
    %{data: render_one(item, Rojak.ItemView, "item.json")}
  end

  def render("item.json", %{item: item}) do
    %{
      id: item.id,
      name: item.name,
      image: item.image,
      race: item.race,
      halal: item.halal,
      veg: item.veg,
      description: item.description}
  end
end
