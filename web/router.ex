defmodule Rojak.Router do
  use Rojak.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", Rojak do
    pipe_through :api

    resources "/users", UserController, only: [:create]
    resources "/sessions", SessionsController, only: [:create, :delete]
    resources "/items", ItemController, except: [:new, :edit]
    post "/food", ItemController, :get_item
  end

  scope "/", Rojak do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", Rojak do
  #   pipe_through :api
  # end
end
