defmodule Rojak.Item do
  use Rojak.Web, :model

  schema "items" do
    field :s, :string
    field :name, :string
    field :image, :string
    field :race, :string
    field :halal, :integer
    field :veg, :integer
    field :description, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :image, :race, :halal, :veg, :description])
    |> validate_required([:name, :image, :race, :halal, :veg, :description])
  end
end
