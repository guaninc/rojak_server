defmodule Rojak.Session do
  use Rojak.Web, :model

  schema "sessions" do
    field :email, :string, virtual: true
    field :password, :string, virtual: true
    field :session_id, :string

    belongs_to :user, Rojak.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:session_id])
    |> validate_required([:session_id])
  end
end
