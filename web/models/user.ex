defmodule Rojak.User do
  use Rojak.Web, :model

  schema "users" do
    field :email, :string
    field :name, :string
    field :password, :string
    field :halal, :integer
    field :veg, :integer
    field :race, :string

    has_many :sessions, Rojak.Session

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :name, :password, :halal, :veg, :race])
    |> validate_required([:email, :name, :password, :halal, :veg, :race])
    |> validate_inclusion(:race, ["chinese", "indian", "malay", "others"])
    |> unique_constraint(:email, name: :email_constraint)
  end
end
