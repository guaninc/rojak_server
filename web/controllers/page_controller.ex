defmodule Rojak.PageController do
  use Rojak.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
