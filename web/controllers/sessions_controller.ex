defmodule Rojak.SessionsController do
  use Rojak.Web, :controller

  alias Rojak.Session
  alias Rojak.User

  def index(conn, _params) do
    email = Repo.all(Sessions)
    render(conn, "index.json", email: email)
  end

  def create(conn, %{"sessions" => sessions_params}) do
    email = sessions_params["email"]
    password = sessions_params["password"]

    case Repo.get_by(User, email: email) do
      nil ->
        conn
        |> send_resp(404, "")
      user ->
        if password == user.password do
          session_id = Base.encode16(:crypto.strong_rand_bytes(16))
          changeset =
            user
            |> build_assoc(:sessions)
            |> Session.changeset(%{
                session_id: session_id
              })
          sess = Repo.insert! changeset
          sess = Repo.get!(Session, sess.id) |> Repo.preload([:user])

          conn
          |> put_status(:ok)
          |> render("show.json", sessions: sess)
        else
          conn
          |> send_resp(404, "")
        end
    end
  end

  def show(conn, %{"id" => id}) do
    sessions = Repo.get!(Sessions, id)
    render(conn, "show.json", sessions: sessions)
  end

  def update(conn, %{"id" => id, "sessions" => sessions_params}) do
    sessions = Repo.get!(Sessions, id)
    changeset = Sessions.changeset(sessions, sessions_params)

    case Repo.update(changeset) do
      {:ok, sessions} ->
        render(conn, "show.json", sessions: sessions)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Rojak.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    sessions = Repo.get!(Sessions, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(sessions)

    send_resp(conn, :no_content, "")
  end
end
