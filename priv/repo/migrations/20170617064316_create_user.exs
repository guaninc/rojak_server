defmodule Rojak.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :name, :string
      add :password, :string
      add :race, :string
      add :halal, :integer
      add :veg, :integer
      add :beef, :integer
      add :nuts, :integer

      timestamps()
    end

    create table(:sessions) do
      add :session_id, :string
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create unique_index(:users, [:email], name: :email_constraint)
  end
end
