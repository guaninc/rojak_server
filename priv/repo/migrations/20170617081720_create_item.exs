defmodule Rojak.Repo.Migrations.CreateItem do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :s, :string
      add :name, :string
      add :image, :string
      add :race, :string
      add :halal, :integer
      add :veg, :integer
      add :description, :string

      timestamps()
    end

  end
end
